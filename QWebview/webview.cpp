#include "webview.h"

WebView::WebView(QWidget *parent) :
    QWebView(parent)
{
    this->load(QUrl("qrc:/html/baidumap.html"));

    this->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    this->resize(800,600);
}
