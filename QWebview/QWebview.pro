#-------------------------------------------------
#
# Project created by QtCreator 2013-07-31T14:41:58
#
#-------------------------------------------------

QT       += core gui webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = webview
TEMPLATE = app


SOURCES += main.cpp \
    webview.cpp

HEADERS  += \
    webview.h

FORMS    +=

RESOURCES += \
    resource.qrc

OTHER_FILES += \
    html/baidumap.html
