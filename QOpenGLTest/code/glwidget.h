#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QGLWidget>

class QTimer;
class QKeyEvent;
class GLWidget : public QGLWidget
{
    Q_OBJECT
public:
    explicit GLWidget(QWidget *parent = 0);

protected:
    virtual void initializeGL();
    virtual void paintGL();
    virtual void resizeGL( int width, int height );
    virtual void timeOut();
    void mousePressEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void wheelEvent(QWheelEvent *e);
    void keyPressEvent( QKeyEvent *e );
    void buildLists();
    void loadGLTextures();
signals:

public slots:
    void timeOutSlot();

private:
    bool fullscreen;

    GLfloat xRot, yRot, zRot;    // 旋转量
    QTimer *m_timer;
    GLfloat scaling;
    GLfloat zoom;
    GLuint texture[6]; // 存储6个纹理

    QPoint lastPos;
    bool light;
    GLuint box, top;
    GLuint xLoop, yLoop;
    GLuint fogFilter;

    int timerInterval;
};

#endif // GLWIDGET_H
