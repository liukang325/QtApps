QT       += core opengl xml
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
DESTDIR = ../bin
TARGET = QOpenGLTest
CONFIG   -= console

TEMPLATE = app

SOURCES += main.cpp \
    glwidget.cpp

HEADERS += \
    glwidget.h

RESOURCES += \
    glwidget.qrc
