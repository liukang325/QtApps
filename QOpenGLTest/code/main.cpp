#include <QApplication>
#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <math.h>
#include "glwidget.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    GLWidget w;
    w.showMaximized();
    return a.exec();
}
