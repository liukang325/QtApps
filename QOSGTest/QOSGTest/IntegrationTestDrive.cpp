﻿#include "IntegrationTestDrive.h"
#include "HUD.h"
#include "TextUtil.h"
#include "ObjectLabel.h"
#include "TabWidget.h"
#include "UIUtil.h"

#include <osgText/Text>
#include <osgDB/ReadFile>
#include <osg/PositionAttitudeTransform>
#include <osgWidget/WindowManager>
#include <osgViewer/ViewerEventHandlers>

IntegrationTestDrive::IntegrationTestDrive(void)
{
}

IntegrationTestDrive::~IntegrationTestDrive(void)
{
}


int IntegrationTestDrive::Test()
{
	osgViewer::Viewer viewer;
	viewer.setUpViewOnSingleScreen();	
	osg::Group *root = createRoot();
	osgViewer::Viewer::Windows windows;
	viewer.getWindows(windows);
	if (windows.empty()) return 1;

	//////////////////////////////////////////////////////////////////////////
	int nScreenWidth, nScreenHeight;
	//UIUtil::GetScreenInfo(nScreenWidth, nScreenHeight);
	osgWidget::WindowManager* wm = new osgWidget::WindowManager(
		&viewer,
		windows[0]->getTraits()->width,//nScreenWidth,
		windows[0]->getTraits()->height,//nScreenHeight,
		MASK_2D //,
		//osgWidget::WindowManager::WM_USE_RENDERBINS
		);	
//	wm->setSize(windows[0]->getTraits()->width/2, windows[0]->getTraits()->height/2);

	//////////////////////////////////////////////////////////////////////////
	const char* TEXT =	"====这里是信息栏===="	;
	osgWidget::Label* label = createLabel(TEXT, 28.0);
	label->setSize(windows[0]->getTraits()->width, 30);

	osgWidget::Box* infoBar = new osgWidget::Box("VBOX", osgWidget::Box::VERTICAL);
	infoBar->addWidget(label);
	infoBar->getBackground()->setColor(0.0f, 1.0f, 0.0f, 0.5f);
	infoBar->attachScaleCallback();
	infoBar->resize(windows[0]->getTraits()->width,  30);
	infoBar->setOrigin(osgWidget::XYCoord(osg::Vec2f(
		0,	windows[0]->getTraits()->height-30)));	
	wm->addChild(infoBar);

	//////////////////////////////////////////////////////////////////////////
	TabWidget* tabWidget = new TabWidget("notebook1");	
	tabWidget->attachMoveCallback();
	wm->addChild(tabWidget);

	return UIUtil::createExample(viewer, wm, root);
}

int IntegrationTestDrive::TestObjectLabelAndHUD()
{
	osgViewer::Viewer viewer;
	viewer.setUpViewOnSingleScreen();	
	osg::Group *root = createRoot();

	osgViewer::Viewer::Windows windows;
	viewer.getWindows(windows);
	if (windows.empty()) return 1;
	
	//////////////////////////////////////////////////////////////////////////
	osg::Geode* geode = createText2D(0,0,windows[0]->getTraits()->width, windows[0]->getTraits()->height);
	HUD::setHUDStateSet(*geode);
	osg::Camera* hudCamera = HUD::create(0,0,windows[0]->getTraits()->width, windows[0]->getTraits()->height);
	hudCamera->addChild(geode);
	hudCamera->setGraphicsContext(windows[0]);
	hudCamera->setViewport(0,0,windows[0]->getTraits()->width, windows[0]->getTraits()->height);
	root->addChild(hudCamera);
	
	viewer.setSceneData(root);
	return viewer.run();	
}

int IntegrationTestDrive::TestObjectLabelAndTabWidget()
{
	osgViewer::Viewer viewer;
	viewer.setUpViewOnSingleScreen();	
	osg::Group *root = createRoot();

	osgViewer::Viewer::Windows windows;
	viewer.getWindows(windows);
	if (windows.empty()) return 1;

	//////////////////////////////////////////////////////////////////////////
	osgWidget::WindowManager* wm = new osgWidget::WindowManager(
		&viewer,
		windows[0]->getTraits()->width,
		windows[0]->getTraits()->height,
		MASK_2D //,
		//osgWidget::WindowManager::WM_USE_RENDERBINS
		);	

	TabWidget* notebook1 = new TabWidget("notebook1");
	TabWidget* notebook2 = new TabWidget("notebook2");

	notebook2->setOrigin(100.0f, 100.0f);

	notebook1->attachMoveCallback();
	notebook2->attachMoveCallback();

	wm->addChild(notebook1);
	wm->addChild(notebook2);

	return osgWidget::createExample(viewer, wm, root);
}

int IntegrationTestDrive::TestObjectLabelAndWidgetLabel()
{
	osgViewer::Viewer viewer;
	viewer.setUpViewOnSingleScreen();	
	osg::Group *root = createRoot();

	osgViewer::Viewer::Windows windows;
	viewer.getWindows(windows);
	if (windows.empty()) return 1;

	//////////////////////////////////////////////////////////////////////////
	osgWidget::WindowManager* wm = new osgWidget::WindowManager(
		&viewer,
		windows[0]->getTraits()->width,
		windows[0]->getTraits()->height,
		MASK_2D //,
		//osgWidget::WindowManager::WM_USE_RENDERBINS
		);	

	TabWidget* notebook1 = new TabWidget("notebook1");
	TabWidget* notebook2 = new TabWidget("notebook2");

	notebook2->setOrigin(100.0f, 100.0f);

	notebook1->attachMoveCallback();
	notebook2->attachMoveCallback();

	wm->addChild(notebook1);
	wm->addChild(notebook2);

	//////////////////////////////////////////////////////////////////////////
	osgWidget::Box*   box    = new osgWidget::Box("HBOX", osgWidget::Box::HORIZONTAL);
	osgWidget::Box*   vbox   = new osgWidget::Box("vbox", osgWidget::Box::VERTICAL);
	const char* LABEL1 =
		"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed\n"
		"do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n"
		"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris\n"
		"nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in..."
		;
	const char* LABEL2 = 
		"...reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla\n"
		"pariatur. Excepteur sint occaecat cupidatat non proident, sunt in \n"
		"culpa qui officia deserunt mollit anim id est laborum. BBBBB"
		;
	osgWidget::Label* label1 = createLabel(LABEL1);
	osgWidget::Label* label2 = createLabel(LABEL2);

	// Setup the labels for horizontal box.
	label1->setPadding(10.0f);
	label2->setPadding(10.0f);

	label1->addSize(21.0f, 22.0f);
	label2->addSize(21.0f, 22.0f);

	label1->setColor(1.0f, 0.5f, 0.0f, 0.0f);
	label2->setColor(1.0f, 0.5f, 0.0f, 0.5f);

	label2->setImage("Images/Brick-Norman-Brown.TGA", true);

	box->addWidget(label1);
	box->addWidget(label2);
	box->attachMoveCallback();
	box->attachScaleCallback();
	box->attachRotateCallback();

	// Setup the labels for the vertical box.
	osgWidget::Label* label3 = createLabel("Label 3", 80);
	osgWidget::Label* label4 = createLabel("Label 4", 60);
	osgWidget::Label* label5 = createLabel("ABCDEFGHIJK", 93);

	label3->setPadding(3.0f);
	label4->setPadding(3.0f);
	label5->setPadding(3.0f);

	label3->setColor(0.0f, 0.0f, 0.5f, 0.5f);
	label4->setColor(0.0f, 0.0f, 0.5f, 0.5f);
	label5->setColor(0.0f, 0.0f, 0.5f, 0.5f);

	//label5->setAlignHorizontal(osgWidget::Widget::HA_LEFT);
	//label5->setAlignVertical(osgWidget::Widget::VA_BOTTOM);

	// Test our label copy construction...
	osgWidget::Label* label6 = osg::clone(label5, "label6", osg::CopyOp::DEEP_COPY_ALL);

	label6->setLabel("abcdefghijklmnopqrs");

	vbox->addWidget(label3);
	vbox->addWidget(label4);
	vbox->addWidget(label5);
	vbox->addWidget(label6);
	vbox->attachMoveCallback();
	vbox->attachScaleCallback();

	vbox->resize();

	// vbox->setVisibilityMode(osgWidget::Window::VM_ENTIRE);
	// vbox->setVisibleArea(50, 50, 500, 200);
	// vbox->setAnchorVertical(osgWidget::Window::VA_TOP);
	// vbox->setAnchorHorizontal(osgWidget::Window::HA_RIGHT);

	// Test our label-in-window copy construction...
	osgWidget::Box* clonedBox = osg::clone(box, "HBOX-new", osg::CopyOp::DEEP_COPY_ALL);

	clonedBox->getBackground()->setColor(0.0f, 1.0f, 0.0f, 0.5f);

	wm->addChild(box);
	wm->addChild(vbox);
	wm->addChild(clonedBox);

	return osgWidget::createExample(viewer, wm, root);
}

osg::Group* IntegrationTestDrive::createRoot()
{
	osg::Group* root = new osg::Group();       
	osg::PositionAttitudeTransform* tankXform = new osg::PositionAttitudeTransform();
	tankXform->setPosition(osg::Vec3d(5, 5, 8));
	tankXform->addChild(osgDB::readNodeFile("t72-tank_des.flt"));
	tankXform->addChild(ObjectLabel::createObjectLabel("tank", 1.0, osg::Vec3(0,0,4), 1.0));
	root->addChild(tankXform);
	return root;
}

osg::Geode* IntegrationTestDrive::createText2D(int x, int y, int width, int height)
{
	osg::Geode* geode = new osg::Geode();
			
	osgText::Text* text = new  osgText::Text;
	TextUtil::createContent(*text, "============================================这里是（平显）信息栏============================================");
	TextUtil::setupProperties(*text, osgText::readFontFile("fonts/xjlFont.ttf"), 28.0, osg::Vec3(x+width/2, y+height, 0.0f));
	geode->addDrawable( text );
	geode->addDrawable(HUD::BoundingBox(geode));	
	return geode;
}

osgWidget::Label* IntegrationTestDrive::createLabel(const std::string& l, unsigned int size) 
{
    osgWidget::Label* label = new osgWidget::Label("", "");

    label->setFont("fonts/xjlFont.ttf");
    label->setFontSize(size);
    label->setFontColor(1.0f, 1.0f, 1.0f, 1.0f);

	osgText::String *string = new osgText::String;
	TextUtil::createContent(*string, l.data());
    label->setLabel(*string);
	
    return label;
}