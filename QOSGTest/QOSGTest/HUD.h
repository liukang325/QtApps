﻿/*
 * Head Up Displays are simple :-)
 * All you need to do is create your text in a subgraph
 * Then place an osg::Camera above the subgraph to create an orthographic projection.
 * Set the Camera's ReferenceFrame to ABSOLUTE_RF to ensure 
 * it remains independent from any external model view matrices.
 * And set the Camera's clear mask to just clear the depth buffer.
 * And finally set the Camera's RenderOrder to POST_RENDER to make sure it's drawn last.
 */
#pragma once
#include <osg/Geode>
#include <osg/Camera>
#include <osg/Geometry>

class HUD
{
public:
	HUD(void);
	~HUD(void);

	static osg::Camera* create(int x, int y, int width, int height);
	static void setHUDStateSet(osg::Node&);
	static osg::StateSet* createHUDStateSet();

	static osg::Geometry* createHUDBGRect();
	static osg::Drawable* BoundingBox(osg::Geode* geode);
};

