#include "UIUtil.h"
#include <osgWidget/ViewerEventHandlers>
#include <osgViewer/ViewerEventHandlers>
#include <osgGA/StateSetManipulator>
/*
#include <Windows.h>
#include <WinGDI.h>
*/
UIUtil::UIUtil(void)
{
}

UIUtil::~UIUtil(void)
{
}

int UIUtil::createExample(osgViewer::Viewer& viewer, WindowManager* wm, osg::Node* node) 
{
	if(!wm) return 1;
	
	viewer.setUpViewOnSingleScreen();	
	/*
	viewer.setUpViewInWindow(
		0,
		0,
		static_cast<int>(wm->getWidth()),
		static_cast<int>(wm->getHeight())
		);
		*/
	osg::Group*  group  = new osg::Group();
	osg::Camera* camera = wm->createParentOrthoCamera();

	group->addChild(camera);

	if(node) group->addChild(node);

	viewer.addEventHandler(new osgWidget::MouseHandler(wm));
	viewer.addEventHandler(new osgWidget::KeyboardHandler(wm));
	viewer.addEventHandler(new osgWidget::ResizeHandler(wm, camera));
	viewer.addEventHandler(new osgWidget::CameraSwitchHandler(wm, camera));
	viewer.addEventHandler(new osgViewer::StatsHandler());
	viewer.addEventHandler(new osgViewer::WindowSizeHandler());
	viewer.addEventHandler(new osgGA::StateSetManipulator(
		viewer.getCamera()->getOrCreateStateSet()
		));

	wm->resizeAllWindows();

	viewer.setSceneData(group);

	return viewer.run();
}
/*
void UIUtil::GetScreenInfo(int &nScreenWidth, int &nScreenHeight)
{
	HDC screenDC;
	screenDC = CreateDC(L"DISPLAY",NULL, NULL, NULL);

	nScreenWidth = GetDeviceCaps(screenDC, HORZRES);
	nScreenHeight = GetDeviceCaps(screenDC, VERTRES);	
}
*/