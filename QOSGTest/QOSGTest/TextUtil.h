﻿#pragma once

#include <osgText/Text>

class TextUtil
{
public:
	static void createContent(osgText::Text& textObject, const char* string);
	static void createContent(osgText::String& stringObject, const char* string);
	static void setupProperties(osgText::Text& textObject, osgText::Font* font, float size, const osg::Vec3& pos );
};