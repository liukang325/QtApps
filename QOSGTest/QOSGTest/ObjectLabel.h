﻿#pragma once
#include <osg/Node>

class ObjectLabel
{
public:
	ObjectLabel(void);
	~ObjectLabel(void);

	static osg::Node* createObjectLabel(const char * text, float size, osg::Vec3 pos, double scale);
};

