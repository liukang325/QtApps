﻿#include "TextUtilTestDrive.h"
#include "HUDTestDrive.h"
#include "ObjectLabelTestDrive.h"
#include "TabWidgetTestDrive.h"
#include "IntegrationTestDrive.h"

#include <osgViewer/Viewer>
#include <osgDB/ReadFile>
#include <osg/Group>
#include <osg/PositionAttitudeTransform>

int main()
{
	/*
	TextUtilTestDrive::Test();
	HUDTestDrive::Test();
	ObjectLabelTestDrive::Test();
	TabWidgetTestDrive::Test();
	IntegrationTestDrive::TestObjectLabelAndHUD();
	IntegrationTestDrive::TestObjectLabelAndTabWidget();
	IntegrationTestDrive::TestObjectLabelAndWidgetLabel();	
	*/
	IntegrationTestDrive::Test();	
}
