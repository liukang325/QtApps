﻿#include "ObjectLabel.h"
#include "TextUtil.h"
#include <osg/AutoTransform>

ObjectLabel::ObjectLabel(void)
{
}


ObjectLabel::~ObjectLabel(void)
{
}

osg::Node* ObjectLabel::createObjectLabel(const char * text, float size, osg::Vec3 pos, double scale)
{
	osgText::Text* label = new osgText::Text();  
	label->setCharacterSize(size);
	label->setFont("/fonts/xjlFont.ttf");
	TextUtil::createContent(*label, text);
	label->setAxisAlignment(osgText::Text::XY_PLANE);
	label->setDrawMode(osgText::Text::TEXT );
	label->setAlignment(osgText::Text::CENTER_TOP);
	label->setPosition(osg::Vec3(0, 0, 0));
	label->setColor(osg::Vec4(0.3f, 1.0f, 0.5f, 1.0f));
	
	osg::Geode* labelGeode = new osg::Geode();  
	labelGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	labelGeode->addDrawable(label);

	osg::AutoTransform* at = new osg::AutoTransform();
	at->setMinimumScale(1.0);
	at->setScale(scale);
	at->setPosition(pos);
	at->setAutoRotateMode(osg::AutoTransform::ROTATE_TO_SCREEN);
	at->setAutoScaleToScreen(false);
	at->addChild(labelGeode);
	return at;
}
