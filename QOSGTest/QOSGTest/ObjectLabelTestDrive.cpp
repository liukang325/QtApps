﻿#include "ObjectLabelTestDrive.h"
#include "ObjectLabel.h"
#include "TextUtil.h"
#include "HUD.h"

#include <osgViewer/Viewer>
#include <osgDB/ReadFile>
#include <osg/PositionAttitudeTransform> 

ObjectLabelTestDrive::ObjectLabelTestDrive(void)
{
}


ObjectLabelTestDrive::~ObjectLabelTestDrive(void)
{
}

int ObjectLabelTestDrive::Test()
{
	osgViewer::Viewer viewer;
	viewer.setSceneData(createRoot());
	return viewer.run();
}

osg::Group* ObjectLabelTestDrive::createRoot()
{
	osg::Group* root = new osg::Group();       

	osg::PositionAttitudeTransform* tankXform = new osg::PositionAttitudeTransform();
	tankXform->setPosition(osg::Vec3d(5, 5, 8));
	tankXform->addChild(osgDB::readNodeFile("cow.osg"));
	tankXform->addChild(ObjectLabel::createObjectLabel("cow", 1.0, osg::Vec3(0,0,4), 1.0));
	root->addChild(tankXform);

	return root;
}
