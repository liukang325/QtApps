﻿#pragma once
#include <osg/Group>
#include <osg/Geode>
#include <osg/Geometry>

class ObjectLabelTestDrive
{
public:
	ObjectLabelTestDrive(void);
	~ObjectLabelTestDrive(void);

	static int Test();
private:
	static osg::Group* createRoot();
	static osg::Geode* createHud();
};
