﻿#pragma once
#include <osg/Geode>

class HUD;

class HUDTestDrive
{
public:
	HUDTestDrive(void);
	~HUDTestDrive(void);

	static int Test();
private:
	static osg::Geode* createText2D(int x, int y, int width, int height);
};

