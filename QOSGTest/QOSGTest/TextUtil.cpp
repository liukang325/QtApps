﻿#include "TextUtil.h"

void TextUtil::setupProperties(osgText::Text& textObject,osgText::Font* font,float size,const osg::Vec3& pos )
{
	textObject.setFont(font);									//字体
	textObject.setPosition(pos);								//位置
	textObject.setCharacterSize(size);							//大小
	//textObject.setCharacterSizeMode(osgText::Text::OBJECT_COORDS);//大小模式	
	textObject.setColor(osg::Vec4(0.0,0.0,0.0,1.0));			//颜色
	//textObject.setAxisAlignment(osgText::Text::XZ_PLANE);		//方向
	textObject.setAlignment(osgText::Text::CENTER_TOP);		//对齐方式
	textObject.setLayout(osgText::TextBase::LEFT_TO_RIGHT);		//输出格式
	textObject.setFontResolution(32, 32);						//分辨率
	textObject.setDrawMode(osgText::Text::TEXT					//绘制模式
		//| osgText::Text::BOUNDINGBOX);				//包围盒
		//| osgText::Text::ALIGMENT						//对齐
		);
	//textObject.setBackdropType(									//背景类型
		//osgText::Text::OUTLINE						//描边
		//osgText::Text::DROP_SHADOW_BOTTOM_RIGHT		//默认阴影
		//);
	//textObject.setBackdropColor(osg::Vec4(0.5,0.5,0.5,0.5));	//背景颜色
	//textObject.setColorGradientMode(osgText::Text::SOLID);		//颜色倾斜模式
	//textObject.setAutoRotateToScreen(true);	
}

//createContent函数：设置osgText文本内容
void TextUtil::createContent(osgText::Text& textObject, const char* string)
{
	setlocale(LC_ALL,"chs");					//支持中文

	int requiredSize=mbstowcs(NULL,string,0);	//如果mbstowcs第一参数为NULL那么返回字符串的数目
	wchar_t* wText=new wchar_t[requiredSize+1];
	mbstowcs(wText,string,requiredSize+1);		//mbstowcs的作用是将char转换成wchar类型
	//注：也可以使用L或_T等方式转换，如textObject.setText(L"hello中文");

	textObject.setText(wText);
	delete wText;
}

//createContent函数：设置osgText文本内容
void TextUtil::createContent(osgText::String& stringObject, const char* string)
{
	setlocale(LC_ALL,"chs");					//支持中文

	int requiredSize=mbstowcs(NULL,string,0);	//如果mbstowcs第一参数为NULL那么返回字符串的数目
	wchar_t* wText=new wchar_t[requiredSize+1];
	mbstowcs(wText,string,requiredSize+1);		//mbstowcs的作用是将char转换成wchar类型
	//注：也可以使用L或_T等方式转换，如textObject.setText(L"hello中文");

	stringObject.set(wText);
	delete wText;
}
