﻿#include "TabWidgetTestDrive.h"
#include "TabWidget.h"

TabWidgetTestDrive::TabWidgetTestDrive(void)
{
}

TabWidgetTestDrive::~TabWidgetTestDrive(void)
{
}

int TabWidgetTestDrive::Test()
{
	osgViewer::Viewer viewer;
	osgWidget::WindowManager* wm = new osgWidget::WindowManager(
		&viewer,1280.0f,720.0f,MASK_2D 
		//,
		//osgWidget::WindowManager::WM_USE_RENDERBINS
		);

	TabWidget* notebook1 = new TabWidget("notebook1");
	TabWidget* notebook2 = new TabWidget("notebook2");

	notebook2->setOrigin(100.0f, 100.0f);

	notebook1->attachMoveCallback();
	notebook2->attachMoveCallback();

	wm->addChild(notebook1);
	wm->addChild(notebook2);

	return osgWidget::createExample(viewer, wm);

}