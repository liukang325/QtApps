﻿#include "TextUtilTestDrive.h"
#include "TextUtil.h"
#include <osg/Geode>
#include <osg/Geometry>
#include <osgText/Text>
#include <osgViewer/Viewer>
#include <osgDB/ReadFile>

TextUtilTestDrive::TextUtilTestDrive(void)
{
}


TextUtilTestDrive::~TextUtilTestDrive(void)
{
}

int TextUtilTestDrive::Test()
{
	osg::ref_ptr<osg::Group> root = new osg::Group();
	root->addChild(creatText2D());

	osgViewer::Viewer viewer;
	viewer.setSceneData(root.get());
	return viewer.run(); 	
}

osg::Geode* TextUtilTestDrive::creatText2D()
{
	const char* titleString="木兰辞\n拟古决绝词简友";
	const char* textString={
		"人生若只如初见，何事秋风悲画扇；\n"
		"等闲变却故人心，却道故人心易变；\n"
		"骊山语罢倾销半，夜雨霖铃终不怨；\n"
		"何如薄幸锦衣郎，比翼连枝当日愿。"
	};
	osg::ref_ptr<osgText::Font> fontXJL = new osgText::Font();
	osg::ref_ptr<osgText::Text> title=new osgText::Text;	
	TextUtil::createContent(*title, titleString);	
	TextUtil::setupProperties(*title, osgText::readFontFile("fonts/xjlFont.ttf"), 20.0f, osg::Vec3(0.0,0.0,0.0));						
	osg::ref_ptr<osgText::Text> text=new osgText::Text;	
	TextUtil::createContent(*text, textString);
	TextUtil::setupProperties(*text,  osgText::readFontFile("fonts/xjlFont.ttf"), 15.0, osg::Vec3(0.0,0.0,-80.0f));
	
	osg::ref_ptr<osg::Geode> geode=new osg::Geode;
	geode->addDrawable(osg::createTexturedQuadGeometry(osg::Vec3(-150.0,1.0,-130.0),osg::Vec3(300.0,0.0,0.0),osg::Vec3(0.0,0.0,200.0),1.0,1.0));//创建一个写字板	
	geode->addDrawable(title.get());
	geode->addDrawable(text.get());
	return geode.get();
}
