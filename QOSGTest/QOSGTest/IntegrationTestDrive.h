﻿#pragma once
#include <osg/Group>
#include <osgWidget/Label>

class IntegrationTestDrive
{
public:
	IntegrationTestDrive(void);
	~IntegrationTestDrive(void);

	static int Test();
	static int TestObjectLabelAndHUD();
	static int TestObjectLabelAndTabWidget();
	static int TestObjectLabelAndWidgetLabel();
private:
	static osg::Group* createRoot();
	static osg::Geode* createText2D(int x, int y, int width, int height);
	static osgWidget::Label* IntegrationTestDrive::createLabel(const std::string& l, unsigned int size=13);
};

