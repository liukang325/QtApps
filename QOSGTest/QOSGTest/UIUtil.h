#include <osgViewer/Viewer>
#include <osgWidget/WindowManager>
#include <osg/Node>
using namespace osgWidget;

class UIUtil
{
public:
	UIUtil(void);
	~UIUtil(void);

	static int createExample(osgViewer::Viewer& viewer, WindowManager* wm, osg::Node* node);
	//static void GetScreenInfo(int &nScreenWidth, int &nScreenHeight);
};
