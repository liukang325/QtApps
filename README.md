项目描述：

QtApps是用Qt写的小程序集合，整理了一下，还是比较杂乱，希望能对自己或者他人以后的编程有一点帮助。

软件环境:
	
Windows7 Ultimate(32bit) / qt-opensource-windows-x86-msvc2010_opengl-5.3.0 / VS2010 Ultimate / qt-vs-addin-1.2.3-opensource

程序目录：

QDesignPattern —— （2012-）若干设计模式的例子，改写自《大话设计模式》和《Head First 设计模式》。

QGUIGenerator —— （2012）解析XML文件，动态生成主窗口，只完成了生成菜单（子菜单），没有继续做工具栏等。

QMouseandKeyevent —— （2012）试验鼠标键盘事件

QInvokeDLL —— （2012）试验调用DLL

QProcessSimulation —— （2012）学习QTime使用，用于飞行过程模拟

QSuffixModifier —— （2012）这个是应测试(yan)的要求，做的批量修改文件后缀名的小程序，原来的代码弄丢了，所以重写的。

QMainCtrl-Component —— （2012-2013）主控+组件结构的例子，使用QtPlugin也就是Qt插件模块做的，这样做的好处是，不需要写组件配置文件。

QDataFilter —— （2014）依据名单过滤特定格式的表（文本文件）中的数据，复制到另一文件。（在某项报名工作中使用，不具有通用性。）

QPickFiles —— （2014）依据名单过滤文件夹中的文件（照片），复制到另一文件夹。（在某项报名工作中使用，不具有通用性。）

QOpenGLTest —— （2014）学习OpenGL所做的试验

QOSGTest —— （2014）学习OSG所做的试验

QMultimediaTest —— （2014）试验Qt多媒体，如QSound

联系方式：
	
如果您有任何问题或者建议，请联系我(e-mail : 18714449762@163.com).
