#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    ui->labelKeyEventType->setText(event->text());

    QString strKey;
    strKey.setNum(event->key());
    ui->labelKeyEventKey->setText(strKey);

    Qt::KeyboardModifiers modifier = event->modifiers();
    QString strModifier;
    strModifier.setNum(modifier);
    ui->labelKeyEventModifiers->setText(strModifier);

}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{

}
