﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTime>

namespace Ui {
class MainWindow;
}

enum PROCESSEMULATE
{
    PROCESSEMULATE_START,
    PROCESSEMULATE_STOP,
    PROCESSEMULATE_PAUSE,
    PROCESSEMULATE_RESUME,
    PROCESSEMULATE_DEFAULT
};

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void start();
    void stop();
    void pause();
    void resume();
    void write(const QString & strText);

protected:
    void timerEvent(QTimerEvent *);

private:
    Ui::MainWindow *ui;

    PROCESSEMULATE      m_nCurrentState;    // 当前仿真状态
    QTime               m_timeEmulate;      // 记录持续时间
    int                 m_nMscsElapsed;     // 开启计时器后经过的毫秒数，中间变量
};

#endif // MAINWINDOW_H
