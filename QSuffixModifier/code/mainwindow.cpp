#include "mainwindow.h"
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QDialog(parent)
{
    suffixModifier = new SuffixModifier(this);

    initWidgets();

    connectSignals();
}

void MainWindow::initWidgets()
{
    layoutSpecifyDir = new QHBoxLayout;
    labelSpecifyDir = new QLabel(QObject::tr("Directory"));
    editDir = new QLineEdit;
    btnSpecifyDir = new QPushButton(QObject::tr("Open"));
    layoutSpecifyDir->addWidget(labelSpecifyDir);
    layoutSpecifyDir->addWidget(editDir);
    layoutSpecifyDir->addWidget(btnSpecifyDir);

    layoutEditSuffix = new QHBoxLayout;
    labelOldSuffix = new QLabel(QObject::tr("Old suffix"), this);
    editOldSuffix = new QLineEdit;
    labelNewSuffix = new QLabel(QObject::tr("New suffix"), this);
    editNewSuffix = new QLineEdit;
    layoutEditSuffix->addWidget(labelOldSuffix);
    layoutEditSuffix->addWidget(editOldSuffix);
    layoutEditSuffix->addWidget(labelNewSuffix);
    layoutEditSuffix->addWidget(editNewSuffix);

    layoutProcessing = new QHBoxLayout;
    progressOfProcessing = new QProgressBar;
    progressOfProcessing->setRange(0, 100);
    btnStart = new QPushButton(QObject::tr("Start"), this);
    layoutProcessing->addWidget(progressOfProcessing);
    layoutProcessing->addWidget(btnStart);

    layoutMain = new QVBoxLayout;
    layoutMain->addLayout(layoutSpecifyDir);
    layoutMain->addLayout(layoutEditSuffix);
    layoutMain->addLayout(layoutProcessing);
    this->setLayout(layoutMain);

    this->adjustSize();
}

void MainWindow::connectSignals()
{
    QObject::connect(btnSpecifyDir, SIGNAL(clicked()), this, SLOT(specifyDir()));
    QObject::connect(btnStart, SIGNAL(clicked()), this, SLOT(startProcessing()));
    QObject::connect(suffixModifier, SIGNAL(processingProgress(int)), this, SLOT(setProgressValue(int)));
}

void MainWindow::specifyDir()
{
    QFileDialog openFileDialog(this, QObject::tr("Specify a directory") );
    openFileDialog.setFileMode(QFileDialog::DirectoryOnly);
    openFileDialog.exec();
    QStringList strDirList = openFileDialog.selectedFiles();
    QString strText;
    for(int i = 0; i < strDirList.count(); ++i)
    {
        QString strDir = strDirList.at(i);
        if (QDir(strDir).isReadable())
        {
            strText.append(strDir);
            if (i > 0)
            {
                strText.append(";");
            }
        }
        else
        {
            QMessageBox::warning(this, QObject::tr("Warning"),
                                 QObject::tr("A directory you have selected is not readable!"));
        }
    }
    editDir->setText(strText);
}

void MainWindow::startProcessing()
{
    QString strOldSuffix = editOldSuffix->text();
    QString strNewSuffix = editNewSuffix->text();
    if (strOldSuffix.isEmpty() && strNewSuffix.isEmpty())
    {
        QMessageBox::warning(this, QObject::tr("Warning"),
                          QObject::tr("Old suffix and new suffix are both empty!"));
    }
    else if(QString::compare(strOldSuffix, strNewSuffix, Qt::CaseInsensitive) == 0)
    {
        QMessageBox::information(this, QObject::tr("Information"),
                          QObject::tr("Old suffix and new suffix are same!"));
    }
    QStringList strDirList = editDir->text().split(";");
    if (strDirList.count() == 0)
    {
        QMessageBox::information(this, QObject::tr("Information"),
                          QObject::tr("No directories need to be addressed!"));
    }

    for(int i = 0; i < strDirList.count(); ++i)
    {
        QString strDir = strDirList.at(i);
        suffixModifier->replace(strDir, strOldSuffix, strNewSuffix);
    }
}

void MainWindow::setProgressValue(int i)
{
    progressOfProcessing->setValue(i);
}
