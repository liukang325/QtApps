#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QProgressBar>
#include "suffixmodifier.h"

class MainWindow : public QDialog
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    
signals:
    
public slots:
    void specifyDir();
    void startProcessing();
    void setProgressValue(int i);
    
private:
    void initWidgets();
    void connectSignals();

    QVBoxLayout     *layoutMain;
    QHBoxLayout     *layoutSpecifyDir;
    QLabel          *labelSpecifyDir;
    QLineEdit       *editDir;
    QPushButton     *btnSpecifyDir;
    QHBoxLayout     *layoutEditSuffix;
    QLabel          *labelOldSuffix;
    QLineEdit       *editOldSuffix;
    QLabel          *labelNewSuffix;
    QLineEdit       *editNewSuffix;
    QHBoxLayout     *layoutProcessing;
    QProgressBar    *progressOfProcessing;
    QPushButton     *btnStart;

    SuffixModifier  *suffixModifier;
};

#endif // MAINWINDOW_H
