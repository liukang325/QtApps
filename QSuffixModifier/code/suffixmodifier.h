#ifndef SUFFIXMODIFIER_H
#define SUFFIXMODIFIER_H

#include <QObject>
#include <QString>
#include <QStringList>

class SuffixModifier : public QObject
{
    Q_OBJECT
public:
    explicit SuffixModifier(QObject *parent = 0);
    
    // change the suffix of files in directory "strDir"(or subdirectory)
    // from "strOldSuffix" to "strNewSuffix"
    void replace(const QString &strDir, const QString &strOldSuffix, const QString &strNewSuffix);

signals:
    void processingProgress(int);
    
public slots:
    
};

#endif // SUFFIXMODIFIER_H
