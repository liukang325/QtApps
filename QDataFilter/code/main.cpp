#include <QCoreApplication>
#include <QDebug>
#include "qdatafilter.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    if(argc!=4)
    {
        qDebug() << ("参数个数不正确");
    }
    else
    {
       QDataFilter dataFilter;
       dataFilter.Filter(argv[1], argv[2], argv[3]);
       qDebug() << "处理完成";
       qDebug() << "查找文件：" << qPrintable(argv[1]);
       qDebug() << "查找内容在：" << qPrintable(argv[2]);
       qDebug() << "结果保存在：" << qPrintable(argv[3]);
    }

    return a.exec();
}
