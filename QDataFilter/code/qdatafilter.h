#ifndef QDATAFILTER_H
#define QDATAFILTER_H

#include <QObject>

class QDataFilter : public QObject
{
    Q_OBJECT
public:
    explicit QDataFilter(QObject *parent = 0);

    //strSeekFrom:需要过滤的文件
    //strSeekInfo:查找的信息（如关键词）
    QStringList Filter(QString strFnSeekFrom, QString strSeekInfo);

    //strSeekFrom:需要过滤的文件
    //strSeekInfo:查找的信息文件
    //strOutput:输出的文件路径
    void Filter(QString strFnSeekFrom, QString strFnSeekInfo, QString strFnOutput);
signals:

public slots:

};

#endif // QDATAFILTER_H
