#-------------------------------------------------
#
# Project created by QtCreator 2014-05-21T14:03:38
#
#-------------------------------------------------

QT       += core

QT       -= gui

DESTDIR = ../bin
TARGET = QDataFilter
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    qdatafilter.cpp

HEADERS += \
    qdatafilter.h
