#-------------------------------------------------
#
# Project created by QtCreator 2014-05-27T19:14:42
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
DESTDIR = ../bin
TARGET = QMultimediaTest
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h

RESOURCES += \
    sound.qrc
