#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
class QPushButton;
class QSound;

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

private:
    QPushButton *btn;
    QSound *sound;
};

#endif // WIDGET_H
