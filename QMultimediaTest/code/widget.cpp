#include "widget.h"
#include <QPushButton>
#include <QSound>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    btn = new QPushButton("play", this);
    sound = new QSound(":/sound/win.wav");
    connect(btn, SIGNAL(clicked()), sound, SLOT(play()));
}

Widget::~Widget()
{

}
