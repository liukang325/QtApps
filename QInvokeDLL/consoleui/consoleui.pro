#-------------------------------------------------
#
# Project created by QtCreator 2012-06-17T18:31:16
#
#-------------------------------------------------

QT       += core

QT       -= gui

DESTDIR  = ..\bin
TARGET = consoleui
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app
LIBS    += -L\consoleapp

SOURCES += main.cpp
