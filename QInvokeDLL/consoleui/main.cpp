#include <QtCore/QCoreApplication>
#include <QLibrary>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    typedef double (*ADD)(double, double);
    QLibrary lib("consoleapp.dll");
    bool bLoadLib = lib.load();
    if (bLoadLib)
    {

        ADD add = (ADD)lib.resolve("add");
        double dA = 1.3;
        double dB = 2.2;
        if (add)
        {
            qDebug() << "a+b=" << add(dA, dB);
        }
        else
        {
            qDebug() << "resolve failed!";
        }

        lib.unload();

    }


    return a.exec();
}
