#ifndef OPERATORADD_H
#define OPERATORADD_H

#ifdef Q_WS_WIN
#define MY_EXPORT __declspec(dllexport)
#else
#define MY_EXPORT
#endif

class OperatorAdd
{
public:
    OperatorAdd(double dNumLeft, double dNumRight);

    double result();
private:
    double m_dNumLeft;
    double m_dNumRight;
};

extern "C" MY_EXPORT double add(double dA, double dB)
{
    OperatorAdd op(dA, dB);
    return op.result();
}

#endif // OPERATORADD_H
