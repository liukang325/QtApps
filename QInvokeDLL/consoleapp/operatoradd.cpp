#include "operatoradd.h"

OperatorAdd::OperatorAdd(double dNumLeft, double dNumRight)
{
    m_dNumLeft = dNumLeft;
    m_dNumRight = dNumRight;
}

double OperatorAdd::result()
{
    return m_dNumLeft + m_dNumRight;
}
