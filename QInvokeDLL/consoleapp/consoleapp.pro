#-------------------------------------------------
#
# Project created by QtCreator 2012-06-17T18:20:13
#
#-------------------------------------------------

QT       += core

QT       -= gui

DESTDIR = ..\bin
TARGET = consoleapp
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = lib


SOURCES += main.cpp \
    operatoradd.cpp

HEADERS += \
    operatoradd.h
