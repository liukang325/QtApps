#-------------------------------------------------
#
# Project created by QtCreator 2014-04-09T09:12:59
#
#-------------------------------------------------

QT       += core

QT       -= gui
DESTDIR = ../bin
TARGET = QPickFiles
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    qpickfiles.cpp

HEADERS += \
    qpickfiles.h
