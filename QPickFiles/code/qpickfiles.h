#ifndef QPICKFILES_H
#define QPICKFILES_H

#include <QObject>
#include <QString>

class QPickFiles : public QObject
{
    Q_OBJECT
public:
    explicit QPickFiles(QObject *parent = 0);

    QString PickFiles(QString strDirIn, QString strDirOut, QString strSeekInfo);
    void PickFiles(QString strDirIn, QString strDirOut, QString strSeekInfoFn, QString strDebugInfoFn);
signals:

public slots:

};

#endif // QPICKFILES_H
