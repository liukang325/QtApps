#include "qpickfiles.h"
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QTextStream>

QPickFiles::QPickFiles(QObject *parent) :
    QObject(parent)
{
}

QString QPickFiles::PickFiles(QString strDirIn, QString strDirOut, QString strSeekInfo)
{
    QString strRes("");
    //要查找的文件名
    QString strTrimmedSeekInfo = strSeekInfo;
    if (strTrimmedSeekInfo.endsWith("\n"))
    {
        strTrimmedSeekInfo.chop(1);
    }
    strTrimmedSeekInfo.trimmed();

    QDir dir(strDirIn);
    QDir dirOut(strDirOut);
    if (!dir.exists())
    {
        qDebug() << "要查找的目录不存在!";
        return strRes;
    }
    QFileInfoList fileInfoList = dir.entryInfoList();
    for(int i = 0; i < fileInfoList.count(); ++i)
    {
        QFileInfo fileInfo = fileInfoList.at(i);
        if(fileInfo.isFile())
        {
            QString strFileName = fileInfo.fileName();
            if (strFileName.contains(strTrimmedSeekInfo))      // 包含要查找的信息即可
            {
                // 复制
                QString strNewFilePath = dirOut.absolutePath();
                strNewFilePath.append("\\");
                strNewFilePath.append(strFileName);
                QFile::copy(fileInfo.filePath(), strNewFilePath);

                strRes = QString("复制到："+strNewFilePath);
                return strRes;
            }
        }
    }
    strRes = QString(strTrimmedSeekInfo+"未找到");
    return strRes;
}

void QPickFiles::PickFiles(QString strDirIn, QString strDirOut, QString strSeekInfoFn, QString strDebugInfoFn)
{
    QFile seekInfoFile(strSeekInfoFn);
    QFile debugInfoFile(strDebugInfoFn);
    QTextStream out(&debugInfoFile);
    if(!seekInfoFile.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "照片信息文件不存在!";
        return;
    }
    else
    {
        if(!debugInfoFile.open(QFile::WriteOnly | QFile::Text))
        {
            qDebug() << "无法写入查找结果!";
            return;
        }
        QString strSeekInfo;
        QString strDebugInfo;
        while(!seekInfoFile.atEnd())
        {
            strSeekInfo = seekInfoFile.readLine();
            //qDebug() << QStringLiteral("查找-") << strSeekInfo;
            strDebugInfo = PickFiles(strDirIn, strDirOut, strSeekInfo);
            out << strDebugInfo << endl;
        }
    }
}
