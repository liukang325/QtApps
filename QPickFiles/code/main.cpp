#include <QCoreApplication>
#include <QDebug>
#include "qpickfiles.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    if(argc!=5)
    {
        qDebug() << ("请输入想要编辑的文件名如:fillname");
    }
    else
    {
       QPickFiles pickFiles;
       pickFiles.PickFiles(argv[1], argv[2], argv[3], argv[4]);
       qDebug() << "处理完成";
       qDebug() << "查找文件夹：" << qPrintable(argv[1]);
       qDebug() << "结果保存在：" << qPrintable(argv[2]);
       qDebug() << "查找内容在：" << qPrintable(argv[3]);
       qDebug() << "查找结果见：" << qPrintable(argv[4]);
    }

    return a.exec();
}
