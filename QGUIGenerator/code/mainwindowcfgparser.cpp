#include "mainwindowcfgparser.h"
#include "mainwindow.h"
#include <QFile>
#include <QString>
#include <QMainWindow>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QToolBar>
#include <QtXml/QDomDocument>

MainWindowCfgParser::MainWindowCfgParser(QObject *parent) :
    QObject(parent)
{
}

MainWindow* MainWindowCfgParser::parse(QString strCfgFilePath)
{
    if (strCfgFilePath == "")
    {
        return NULL;
    }

    m_strCfgFilePath = strCfgFilePath;
    QFile cfgFile(m_strCfgFilePath);
    if (!cfgFile.open(QFile::ReadOnly | QFile::Text))
    {
        return NULL;
    }

    QDomDocument domDocument;
    if (!domDocument.setContent(&cfgFile))
    {
        return NULL;
    }

    QDomElement form = domDocument.documentElement();
    if (form.tagName() != "form")
    {
        return NULL;
    }

    return parseFormElement(form);
}

MainWindow* MainWindowCfgParser::parseFormElement(const QDomElement &element)
{
    m_pMainWindow = new MainWindow;
    if (m_pMainWindow == NULL)
    {
        return NULL;
    }

    QDomElement childElement = element.firstChildElement();
    while (!childElement.isNull())
    {
        if (childElement.tagName() == "menubar")
        {
            //note : menubar element should be unique in form config file
            break;
        }
        childElement = childElement.nextSiblingElement();
    }

    if (!childElement.isNull())
    {
        QMenuBar *pMenuBar = parseMenuBarElement(childElement);
        if (pMenuBar != NULL)
        {
            m_pMainWindow->setMenuBar(pMenuBar);
        }
    }
    m_pMainWindow->adjustSize();

    return m_pMainWindow;
}

QMenuBar* MainWindowCfgParser::parseMenuBarElement(const QDomElement &element)
{
    QMenuBar *pMenuBar = new QMenuBar(m_pMainWindow);
    if (pMenuBar == NULL)
    {
        return NULL;
    }

    connect(pMenuBar, SIGNAL(triggered(QAction*)), m_pMainWindow, SLOT(action(QAction*)));

    QDomElement childElement = element.firstChildElement();
    while (!childElement.isNull())
    {
        if (childElement.tagName() == "menu")
        {
            QMenu *pMenu = parseMenuElement(childElement);
            if (pMenu != NULL)
            {
                pMenuBar->addMenu(pMenu);
            }
        }
        else if(childElement.tagName() == "action")
        {
            QAction *pAction = parseActionElement(childElement);
            if (pAction != NULL)
            {
                pMenuBar->addAction(pAction);

                QString strActionMsg = childElement.attribute("action");
                if (strActionMsg != "")
                {
                    pAction->setProperty("action", strActionMsg);
                }
            }
        }
        childElement = childElement.nextSiblingElement();
    }

    return pMenuBar;
}

QMenu* MainWindowCfgParser::parseMenuElement(const QDomElement &element)
{
    QMenu *pMenu = new QMenu(m_pMainWindow);
    if (pMenu == NULL)
    {
        return NULL;
    }

    QString strMenuTitle = element.attribute("title");
    if (strMenuTitle != "")
    {
        pMenu->setTitle(strMenuTitle);
    }

    //recursive call to parse child menus
    QDomElement childElement = element.firstChildElement();
    while (!childElement.isNull())
    {
        if (childElement.tagName() == "menu")
        {
            QMenu *pChildMenu = parseMenuElement(childElement);
            if (pChildMenu != NULL)
            {
                pMenu->addMenu(pChildMenu);
            }
        }
        else if(childElement.tagName() == "action")
        {
            QAction *pChildAction = parseActionElement(childElement);
            if (pChildAction != NULL)
            {
                pMenu->addAction(pChildAction);
            }

            QString strActionMsg = childElement.attribute("action");
            if (strActionMsg != "")
            {
                pChildAction->setProperty("action", strActionMsg);
            }
        }
        childElement = childElement.nextSiblingElement();
    }

    return pMenu;
}

QAction* MainWindowCfgParser::parseActionElement(const QDomElement &element)
{
    QAction *pAction = new QAction(m_pMainWindow);
    if (pAction == NULL)
    {
        return NULL;
    }

    QString strActionText = element.attribute("text");
    if (strActionText != "")
    {
        pAction->setIconText(strActionText);
    }

    return pAction;
}

QToolBar* MainWindowCfgParser::parseToolBarElement(const QDomElement &element)
{
    QToolBar *pToolBar = new QToolBar(m_pMainWindow);
    if (pToolBar == NULL)
    {
        return NULL;
    }

    //-- begin : parse toolbar ----------------------------------
    //...
    //-- end : parse toolbar ------------------------------------

    return pToolBar;
}
