#include "mainwindow.h"
#include "mainwindowcfgparser.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWindowCfgParser parser;
    MainWindow *w = parser.parse("GUIConfig.xml");
    w->showMaximized();
    
    return a.exec();
}
