#-------------------------------------------------
#
# Project created by QtCreator 2013-08-20T17:24:43
#
#-------------------------------------------------

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

DESTDIR = ../../bin
TARGET = QGUIUtil
TEMPLATE = app


SOURCES += \
    mainwindowcfgparser.cpp \
    mainwindow.cpp \
    main.cpp

HEADERS  += \
    mainwindowcfgparser.h \
    mainwindow.h

OTHER_FILES += \
    ../bin/FormConfig.xml \
    ../README.md
