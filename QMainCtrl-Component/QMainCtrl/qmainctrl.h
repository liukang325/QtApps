#ifndef QMAINCTRL_H
#define QMAINCTRL_H

#include <QObject>

class QComponentInterface;

class QMainCtrl : public QObject
{
    Q_OBJECT
public:
    explicit QMainCtrl(QObject *parent = 0);
    ~QMainCtrl();

    virtual void    init();
    virtual void    release();
    virtual void    executeCommand(const QString &strCommandId);
signals:

public slots:

protected:
    void    registerComponents();
    void    registerComponent(QComponentInterface*);

private:
    QList<QComponentInterface*>         m_componentList;
};

#endif // QMAINCTRL_H
