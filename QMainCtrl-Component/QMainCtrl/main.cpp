#include <QApplication>

#include "qmainctrl.h"

int main(int argv, char *args[])
{
    QApplication app(argv, args);

    QMainCtrl mainCtrl;
    mainCtrl.init();
    mainCtrl.executeCommand("start");

    return app.exec();
}
