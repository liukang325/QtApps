QT             += core
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

DESTDIR         = ../bin
TEMPLATE        = app
TARGET          = QMainCtrl

INCLUDEPATH    += ../QComponentInterface
HEADERS        += \
    qmainctrl.h \
    ../QComponentInterface/qcomponentinterface.h
SOURCES        += main.cpp \
    qmainctrl.cpp


