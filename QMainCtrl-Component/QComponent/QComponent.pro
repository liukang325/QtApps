QT             += core
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
DESTDIR         = ../plugins
TEMPLATE        = lib
TARGET          = qcomponent
CONFIG         += plugin
INCLUDEPATH    += ../QComponentInterface
HEADERS         = \
    qcomponent.h
SOURCES         = \
    qcomponent.cpp

OTHER_FILES += \
    qcomponent.json

