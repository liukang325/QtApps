#include "qcomponent.h"
#include <QDebug>

QComponent::QComponent(QObject *parent) :
    QObject(parent)
{
}

void QComponent::onCommand(const QString &strCommandId)
{
    qDebug() << this->metaObject()->className() << "on command : " << strCommandId;
}

