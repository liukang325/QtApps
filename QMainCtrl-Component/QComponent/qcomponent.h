#ifndef QCOMPONENT_H
#define QCOMPONENT_H

#include <QObject>
#include <QtPlugin>
#include "qcomponentinterface.h"

class QComponent : public QObject, public QComponentInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "QComponent" FILE "qcomponent.json")
    Q_INTERFACES(QComponentInterface)
public:
    explicit QComponent(QObject *parent = 0);

    virtual void    release(){}
    virtual void    onCommand(const QString &strCommandId);
signals:

public slots:
    
};


#endif // QCOMPONENT_H
