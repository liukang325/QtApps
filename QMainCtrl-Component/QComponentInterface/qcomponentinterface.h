#ifndef QCOMPONENTINTERFACE_H
#define QCOMPONENTINTERFACE_H

#include <QString>

class QComponentInterface
{
public:
    virtual ~QComponentInterface() {}
    virtual void    release()=0;
    virtual void    onCommand(const QString &strCommandId)=0;
};

Q_DECLARE_INTERFACE(QComponentInterface, "QComponentInterface")

#endif
