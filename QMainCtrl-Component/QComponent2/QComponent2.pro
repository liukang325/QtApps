QT             += core
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
DESTDIR         = ../plugins
TEMPLATE        = lib
TARGET          = qcomponent2
CONFIG         += plugin
INCLUDEPATH    += ../QComponentInterface
HEADERS         = \
    qcomponent2.h
SOURCES         = \
    qcomponent2.cpp

