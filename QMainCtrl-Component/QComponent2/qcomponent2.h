#ifndef QCOMPONENT2_H
#define QCOMPONENT2_H

#include <QObject>
#include <QtPlugin>
#include "qcomponentinterface.h"

class QComponent2 : public QObject, public QComponentInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "QComponent2" FILE "qcomponent2.json")
    Q_INTERFACES(QComponentInterface)
public:
    explicit QComponent2(QObject *parent = 0);

    virtual void    release(){}
    virtual void    onCommand(const QString &strCommandId);
signals:

public slots:
    
};

#endif // QCOMPONENT2_H
