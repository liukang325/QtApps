#include "qcomponent2.h"
#include <QDebug>

QComponent2::QComponent2(QObject *parent) :
    QObject(parent)
{
}

void QComponent2::onCommand(const QString &strCommandId)
{
    qDebug() << this->metaObject()->className() << "on command : " << strCommandId;
}

