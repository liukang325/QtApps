#include <QtCore/QCoreApplication>
#include <iostream>
#include "stdafx.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // 一份浓缩咖啡
    Beverage *pBeverage = (Beverage *)new Espresso;
    if (pBeverage)
    {
        std::cout << qPrintable(QString("%1 $%2").arg(pBeverage->GetDescription()).arg(pBeverage->Cost()))
                     << std::endl;
        //qDebug() << QString("%1 $%2").arg(pBeverage->GetDescription()).arg(pBeverage->Cost());
    }
    delete pBeverage;

    // 再来一份深度烘焙双倍摩卡奶泡咖啡
    Beverage *pBeverage2 = (Beverage *)new DarkRoast;
    if (pBeverage2)
    {
        pBeverage2 = new Mocha(pBeverage2);
        pBeverage2 = new Mocha(pBeverage2);
        pBeverage2 = new Whip(pBeverage2);
        std::cout << qPrintable(QString("%1 $%2").arg(pBeverage2->GetDescription()).arg(pBeverage2->Cost()))
                     << std::endl;
        //qDebug() << QString("%1 $%2").arg(pBeverage2->GetDescription()).arg(pBeverage2->Cost());
    }
    delete pBeverage2;

    // 最后再来一杯调料为豆浆、摩卡、奶泡的HouseBlend咖啡
    Beverage *pBeverage3 = (Beverage *)new HouseBlend;
    if (pBeverage3)
    {
        pBeverage3 = new Soy(pBeverage3);
        pBeverage3 = new Mocha(pBeverage3);
        pBeverage3 = new Whip(pBeverage3);
        std::cout << qPrintable(QString("%1 $%2").arg(pBeverage3->GetDescription()).arg(pBeverage3->Cost()))
                     << std::endl;
        //qDebug() << QString("%1 $%2").arg(pBeverage3->GetDescription()).arg(pBeverage3->Cost());
    }
    delete pBeverage3;


    return a.exec();
}
