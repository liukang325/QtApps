#include "Mocha.h"

Mocha::Mocha(Beverage *pBeverage)
{
    m_pBeverage = pBeverage;
}

Mocha::~Mocha()
{
    if(m_pBeverage)
        delete m_pBeverage;
    m_pBeverage = NULL;
}

double Mocha::Cost()
{
    double cost = 0.2;
    if(m_pBeverage)
    {
        cost += m_pBeverage->Cost();
    }
    return cost;
}

QString Mocha::GetDescription()
{
    QString description = "Mocha";
    if (m_pBeverage)
    {
        description = m_pBeverage->GetDescription() + ", " + description;
    }
    return description;
}
