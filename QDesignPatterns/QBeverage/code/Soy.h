﻿#ifndef SOY
#define SOY

#include "CondimentDecorator.h"

class Soy : public CondimentDecorator
{

public:
    explicit Soy(Beverage *pBeverage);
    virtual ~Soy();

    virtual double Cost();
    virtual QString GetDescription();

private:
    Beverage* m_pBeverage;

};
#endif //SOY
