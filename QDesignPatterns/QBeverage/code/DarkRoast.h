#ifndef DARKROAST_H
#define DARKROAST_H

#include "Beverage.h"

class DarkRoast : public Beverage
{    
public:
    explicit DarkRoast();
    virtual ~DarkRoast(){}

    virtual double Cost();
    virtual QString GetDescription();
};

#endif // DARKROAST_H
