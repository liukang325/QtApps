#-------------------------------------------------
#
# Project created by QtCreator 2012-04-23T20:20:10
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = qbeverage
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

PRECOMPILED_HEADER += stdafx.h
SOURCES += \
    main.cpp \
    Espresso.cpp \
    Mocha.cpp \
    DarkRoast.cpp \
    Whip.cpp \
    HouseBlend.cpp \
    Soy.cpp

HEADERS += \
    stdafx.h \
    Beverage.h \
    Espresso.h \
    Mocha.h \
    DarkRoast.h \
    CondimentDecorator.h \
    Whip.h \
    HouseBlend.h \
    Soy.h
