#ifndef WHIP_H
#define WHIP_H

#include "CondimentDecorator.h"

class Whip : public CondimentDecorator
{
public:
    explicit Whip(Beverage *pBeverage);
    virtual ~Whip();

    virtual double Cost();
    virtual QString GetDescription();

private:
    Beverage* m_pBeverage;
};

#endif // WHIP_H
