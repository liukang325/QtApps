﻿#ifndef STDAFX_H
#define STDAFX_H

#include <QDebug>
#include "Beverage.h"
#include "CondimentDecorator.h"
#include "DarkRoast.h"
#include "Espresso.h"
#include "HouseBlend.h"
#include "Mocha.h"
#include "Soy.h"
#include "Whip.h"

#endif // STDAFX_H
