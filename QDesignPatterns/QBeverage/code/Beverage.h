#ifndef BEVERAGE_H
#define BEVERAGE_H

class QString;

class Beverage
{
public:
    virtual ~Beverage(){}

    virtual double Cost()=0;
    virtual QString GetDescription()=0;
};

#endif // BEVERAGE_H
