﻿///////////////////////////////////////////////////////////
//  Soy.cpp
//  Implementation of the Class Soy
//  Created on:      30-五月-2014 4:50:16
//  Original author: weixl91
///////////////////////////////////////////////////////////

#include "Soy.h"


Soy::Soy(Beverage *pBeverage)
{
    m_pBeverage = pBeverage;
}

Soy::~Soy()
{
    if(m_pBeverage)
        delete m_pBeverage;
    m_pBeverage = NULL;
}

double Soy::Cost()
{
    double cost = 0.15;
    if(m_pBeverage)
    {
        cost += m_pBeverage->Cost();
    }
    return cost;
}

QString Soy::GetDescription()
{
    QString description = "Soy";
    if (m_pBeverage)
    {
        description = m_pBeverage->GetDescription() + ", " + description;
    }
    return description;
}
