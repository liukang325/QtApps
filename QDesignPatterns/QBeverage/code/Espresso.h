#ifndef ESPRESSO_H
#define ESPRESSO_H

#include "Beverage.h"

class Espresso : public Beverage
{
public:
    explicit Espresso();
    virtual ~Espresso(){}

    virtual double Cost();
    virtual QString GetDescription();
};

#endif // ESPRESSO_H
