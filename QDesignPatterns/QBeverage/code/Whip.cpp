#include "Whip.h"

Whip::Whip(Beverage *pBeverage)
{
    m_pBeverage = pBeverage;
}

Whip::~Whip()
{
    if(m_pBeverage)
        delete m_pBeverage;
    m_pBeverage = NULL;
}

double Whip::Cost()
{
    double cost = 0.1;
    if(m_pBeverage)
    {
        cost += m_pBeverage->Cost();
    }
    return cost;
}

QString Whip::GetDescription()
{
    QString description = "Whip";
    if (m_pBeverage)
    {
        description = m_pBeverage->GetDescription() + ", " + description;
    }
    return description;
}
