#ifndef CONDIMENTDECORATOR_H
#define CONDIMENTDECORATOR_H

#include "Beverage.h"

class CondimentDecorator : public Beverage
{
public:
    virtual ~CondimentDecorator(){}

    virtual double Cost()=0;
    virtual QString GetDescription()=0;
};

#endif // CONDIMENTDECORATOR_H
