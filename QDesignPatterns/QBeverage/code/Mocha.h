#ifndef MOCHA_H
#define MOCHA_H

#include "CondimentDecorator.h"

class Mocha : public CondimentDecorator
{
public:
    explicit Mocha(Beverage *pBeverage);
    virtual ~Mocha();

    virtual double Cost();
    virtual QString GetDescription();

private:
    Beverage* m_pBeverage;
};

#endif // MOCHA_H
