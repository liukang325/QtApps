﻿#ifndef HOUSEBLEND
#define HOUSEBLEND

#include "Beverage.h"

class HouseBlend : public Beverage
{
public:
    explicit HouseBlend();
    virtual ~HouseBlend(){}

    virtual double Cost();
    virtual QString GetDescription();

};
#endif //HOUSEBLEND
