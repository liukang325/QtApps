#-------------------------------------------------
#
# Project created by QtCreator 2013-08-31T21:39:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QCashSupermarket
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h \
    ui_widget.h
