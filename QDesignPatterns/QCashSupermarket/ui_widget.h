/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created: Sat Mar 31 23:34:54 2012
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include <QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayoutMain;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *lineEditPricePerUnit;
    QPushButton *pushButton;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *lineEditAmount;
    QPushButton *pushButton_2;
    QListWidget *listWidget;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QLabel *labelTotalPrice;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QString::fromUtf8("Widget"));
        Widget->resize(300, 300);
        verticalLayoutWidget = new QWidget(Widget);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 10, 281, 281));
        verticalLayoutMain = new QVBoxLayout(verticalLayoutWidget);
        verticalLayoutMain->setSpacing(0);
        verticalLayoutMain->setContentsMargins(11, 11, 11, 11);
        verticalLayoutMain->setObjectName(QString::fromUtf8("verticalLayoutMain"));
        verticalLayoutMain->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(verticalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        lineEditPricePerUnit = new QLineEdit(verticalLayoutWidget);
        lineEditPricePerUnit->setObjectName(QString::fromUtf8("lineEditPricePerUnit"));

        horizontalLayout->addWidget(lineEditPricePerUnit);

        pushButton = new QPushButton(verticalLayoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout->addWidget(pushButton);


        verticalLayoutMain->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(verticalLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        lineEditAmount = new QLineEdit(verticalLayoutWidget);
        lineEditAmount->setObjectName(QString::fromUtf8("lineEditAmount"));

        horizontalLayout_2->addWidget(lineEditAmount);

        pushButton_2 = new QPushButton(verticalLayoutWidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        horizontalLayout_2->addWidget(pushButton_2);


        verticalLayoutMain->addLayout(horizontalLayout_2);

        listWidget = new QListWidget(verticalLayoutWidget);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));

        verticalLayoutMain->addWidget(listWidget);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_3 = new QLabel(verticalLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy);

        horizontalLayout_3->addWidget(label_3);

        labelTotalPrice = new QLabel(verticalLayoutWidget);
        labelTotalPrice->setObjectName(QString::fromUtf8("labelTotalPrice"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(labelTotalPrice->sizePolicy().hasHeightForWidth());
        labelTotalPrice->setSizePolicy(sizePolicy1);
        labelTotalPrice->setLineWidth(1);
        labelTotalPrice->setTextFormat(Qt::AutoText);

        horizontalLayout_3->addWidget(labelTotalPrice);


        verticalLayoutMain->addLayout(horizontalLayout_3);


        QMetaObject::connectSlotsByName(Widget);
    } // setupUi
};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
