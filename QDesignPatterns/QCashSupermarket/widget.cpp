﻿#include "widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent), ui(new Ui::Widget)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(on_pushButton_clicked));
}

Widget::~Widget()
{
    delete ui;
}

double dTotalPrice = 0.0;

void Widget::on_pushButton_clicked()
{
    QString strPricePerUnit = ui->lineEditPricePerUnit->text();
    QString strAmout = ui->lineEditAmount->text();

    double dPrice = strPricePerUnit.toDouble() * strAmout.toDouble();
    QString strPrice;
    strPrice.setNum(dPrice);
    dTotalPrice += dPrice;

    QString strItem = tr("单价：") + strPricePerUnit + tr(" 数量：") + strAmout + tr(" 合计：") + strPrice;
    ui->listWidget->addItem(strItem);

    QString strTotalPrice;
    strTotalPrice.setNum(dTotalPrice);
    ui->labelTotalPrice->setText(strTotalPrice);

}
