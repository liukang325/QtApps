﻿/*************************************************
 * Name     : Calculator
 * Function : 输入两个数和运算符号，得到结果
 * Author   : Firmer
 * E-Mail   : mrfirmer@gmail.com
 * Date     : 2012-3-31
 * Version  : v0.3
 *************************************************/
#include <QtCore/QCoreApplication>
#include <iostream>
#include "operation.h"
#include "operationcreator.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    double dA;
    double dB;
    double dResult;
    char chOperator;
    std::cout <<"请输入数字A：";
    std::cin >> dA;
    std::cout << "请选择运算符号(+、-、*、/):";
    std::cin >> chOperator;
    std::cout << "请输入数字B：";
    std::cin >> dB;

    Operation* op;
    op = OperationCreator::creatOperation(chOperator);
    if (op == 0)
    {
        std::cerr << "运算符错误！";
    }
    op->setNumberA(dA);
    op->setNumberB(dB);
    dResult = op->GetResult();

    std::cout << "结果是：" << dResult << std::endl;

    return a.exec();
}
