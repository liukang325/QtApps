#include "operationcreator.h"
#include "operationadd.h"
#include "operationsub.h"
#include "operationmul.h"
#include "operationdiv.h"

OperationCreator::OperationCreator()
{
}

Operation* OperationCreator::creatOperation(char chOperator)
{
    Operation* op = 0;

    switch(chOperator)
    {
    case '+':
        op = new OperationAdd();
        break;
    case '-':
        op = new OperationSub();
        break;
    case '*':
        op = new OperationMul();
        break;
    case '/':
        op = new OperationDiv();
        break;
    default:
        op = 0;
    }

    return op;
}
