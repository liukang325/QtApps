#include "operation.h"

Operation::Operation()
{
}

double Operation::numberA()
{
    return m_numberA;
}

double Operation::numberB()
{
    return m_numberB;
}

void Operation::setNumberA(double numberA)
{
    m_numberA = numberA;
}

void Operation::setNumberB(double numberB)
{
    m_numberB = numberB;
}
