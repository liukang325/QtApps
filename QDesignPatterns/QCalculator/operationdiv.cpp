#include "operationdiv.h"

OperationDiv::OperationDiv()
{
}

double OperationDiv::GetResult()
{
    double dResult = 0.0;
    if (0 == numberB())
    {
        throw ("除数不能为0.");
    }
    dResult = numberA() / numberB();
    return dResult;
}
