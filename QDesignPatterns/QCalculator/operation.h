#ifndef OPERATION_H
#define OPERATION_H

class Operation
{
public:
    Operation();

    virtual double GetResult() = 0;
    double numberA();
    double numberB();
    void setNumberA(double numberA);
    void setNumberB(double numberB);

private:
    double m_numberA;
    double m_numberB;
};

#endif // OPERATION_H
