#ifndef OPERATIONCREATOR_H
#define OPERATIONCREATOR_H
#include "operation.h"

class OperationCreator
{
public:
    OperationCreator();

    static Operation* creatOperation(char chOperator);
};

#endif // OPERATIONCREATOR_H
