#-------------------------------------------------
#
# Project created by QtCreator 2012-03-31T21:56:27
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = QCalculator
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    operation.cpp \
    operationadd.cpp \
    operationsub.cpp \
    operationcreator.cpp \
    operationmul.cpp \
    operationdiv.cpp

HEADERS += \
    operation.h \
    operationadd.h \
    operationsub.h \
    operationcreator.h \
    operationmul.h \
    operationdiv.h
