#ifndef OPERATIONSUB_H
#define OPERATIONSUB_H
#include "operation.h"

class OperationSub : public Operation
{
public:
    OperationSub();

    double GetResult();
};

#endif // OPERATIONSUB_H
