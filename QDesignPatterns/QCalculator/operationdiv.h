#ifndef OPERATIONDIV_H
#define OPERATIONDIV_H
#include "operation.h"

class OperationDiv : public Operation
{
public:
    OperationDiv();

    double GetResult();
};

#endif // OPERATIONDIV_H
