#ifndef OPERATIONMUL_H
#define OPERATIONMUL_H
#include "operation.h"

class OperationMul : public Operation
{
public:
    OperationMul();

    double GetResult();
};

#endif // OPERATIONMUL_H
