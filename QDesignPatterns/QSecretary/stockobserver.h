#ifndef STOCKOBSERVER_H
#define STOCKOBSERVER_H

#include "observer.h"
#include "secretary.h"
#include <QString>

class Secretary;

class StockObserver : public Observer
{
public:
    StockObserver(QString name, Secretary *sub);
    virtual ~StockObserver();

    virtual void Update();
private:
    QString name;
    Secretary *sub;
};

#endif // STOCKOBSERVER_H
