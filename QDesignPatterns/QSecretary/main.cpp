﻿#include <QtCore/QCoreApplication>
#include "secretary.h"
#include "stockobserver.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    //前台小姐童子喆
    Secretary *tongzizhe    = new Secretary();

    //看股票的同事
    StockObserver* yangtianyu       = new StockObserver("Yang Tianyu", tongzizhe);
    StockObserver* sunhongchuang    = new StockObserver("Sun Hongchuang", tongzizhe);

    //前台记下两位同事
    tongzizhe->Attach(yangtianyu);
    tongzizhe->Attach(sunhongchuang);

    //童子喆和孙洪闯生气了
    tongzizhe->Detach(sunhongchuang);

    //发现老板回来
    tongzizhe->action = "The boss is back!";

    //通知同事
    tongzizhe->Notify();

    return a.exec();
}
