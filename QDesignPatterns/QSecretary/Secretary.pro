#-------------------------------------------------
#
# Project created by QtCreator 2012-04-23T20:20:10
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = secretary
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    secretary.cpp \
    stockobserver.cpp \
    observer.cpp \
    subject.cpp

HEADERS += \
    secretary.h \
    stockobserver.h \
    observer.h \
    subject.h
