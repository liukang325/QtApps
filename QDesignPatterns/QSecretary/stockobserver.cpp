#include "stockobserver.h"
#include <iostream>

StockObserver::StockObserver(QString name, Secretary *sub)
{
    this->name  = name;
    this->sub   = sub;
}

StockObserver::~StockObserver()
{

}

void StockObserver::Update()
{
    std::cout << qPrintable(sub->action) << " ";
    std::cout << qPrintable(this->name) << " " << "close the browser, and go to work.\n";
}
