#ifndef SUBJECT_H
#define SUBJECT_H

#include "observer.h"
#include <QList>

class Observer;

class Subject
{
public:
    virtual ~Subject();
    virtual void Attach(Observer *observer)=0;
    virtual void Detach(Observer *observer)=0;
    virtual void Notify()=0;
    //virtual void SetState(const State& st) = 0;
    //virtual State GetState() = 0;
};

#endif // SUBJECT_H
