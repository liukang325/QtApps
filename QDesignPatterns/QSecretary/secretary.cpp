#include "secretary.h"

Secretary::Secretary()
{
    m_pObservers = new QList<Observer*>;
}

Secretary::~Secretary()
{
}

void Secretary::Attach(Observer *observer)
{
    m_pObservers->append(observer);
}

void Secretary::Detach(Observer *observer)
{
    m_pObservers->removeOne(observer);
}

void Secretary::Notify()
{
    foreach (Observer *observer, *m_pObservers)
    {
        observer->Update();
    }
}
