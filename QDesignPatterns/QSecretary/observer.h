#ifndef OBSERVER_H
#define OBSERVER_H
#include "subject.h"

class Subject;

class Observer
{
public:
    virtual ~Observer();
    virtual void Update() = 0;
};

#endif // OBSERVER_H
