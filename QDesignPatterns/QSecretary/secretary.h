#ifndef SECRETARY_H
#define SECRETARY_H

#include "subject.h"
#include "observer.h"
#include <QString>
#include <QList>

class Observer;

class Secretary : public Subject
{
public:
    Secretary();
    virtual ~Secretary();
    virtual void Attach(Observer *observer);
    virtual void Detach(Observer *observer);
    virtual void Notify();
    QString action;
private:
    QList<Observer*> *m_pObservers;
};

#endif // SECRETARY_H
