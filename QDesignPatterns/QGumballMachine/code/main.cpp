#include <QCoreApplication>
#include "GumballMachine.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    GumballMachine gumballMachine(5);

    //System.out.println(gumballMachine);

    gumballMachine.insertQuarter();
    gumballMachine.turnCrank();
    gumballMachine.insertQuarter();
    gumballMachine.insertQuarter();
    gumballMachine.turnCrank();

    //System.out.println(gumballMachine);

    gumballMachine.insertQuarter();
    gumballMachine.insertQuarter();
    gumballMachine.ejectQuarter();

    //System.out.println(gumballMachine);

    gumballMachine.insertQuarter();
    gumballMachine.turnCrank();
    gumballMachine.turnCrank();
    gumballMachine.insertQuarter();
    gumballMachine.turnCrank();

    return a.exec();
}
