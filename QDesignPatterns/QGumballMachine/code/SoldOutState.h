#ifndef SOLDOUTSTATE_H
#define SOLDOUTSTATE_H

#include "State.h"
#include "GumballMachine.h"

class SoldOutState : public State
{
public:
    SoldOutState(GumballMachine *gumballMachine);
    virtual void insertQuarter();
    virtual void ejectQuarter();
    virtual void turnCrank();
    virtual void dispense();
    virtual void refill();
/*
    public String toString() {
        return "sold out";
    }
*/
private:
        GumballMachine *gumballMachine;
};

#endif // SOLDOUTSTATE_H
