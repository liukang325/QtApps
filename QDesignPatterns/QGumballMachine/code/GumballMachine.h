#ifndef GUMBALLMACHINE_H
#define GUMBALLMACHINE_H

#include "State.h"

class GumballMachine
{
public:
    GumballMachine(int numberGumballs);
    void insertQuarter();
    void ejectQuarter();
    void turnCrank();
    void setState(State* state);
    void releaseBall();
    int getCount();
    void refill(int count);
    State* getState();
    State* getSoldOutState();
    State* getNoQuarterState();
    State* getHasQuarterState();
    State* getSoldState();
    State* getWinnerState();
    //QString toString();

private:
    State* soldOutState;
    State* noQuarterState;
    State* hasQuarterState;
    State* soldState;
    State* winnerState;
    State* state;
    int count;
};

#endif // GUMBALLMACHINE_H
