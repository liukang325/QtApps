#include "SoldOutState.h"
#include <QDebug>

SoldOutState::SoldOutState(GumballMachine *gumballMachine) {
    this->gumballMachine = gumballMachine;
}

void SoldOutState::insertQuarter()
{
    qDebug() << QString("You can't insert a quarter, the machine is sold out");
}

void SoldOutState::ejectQuarter()
{
    qDebug() << QString("You can't eject, you haven't inserted a quarter yet");
}

void SoldOutState::turnCrank()
{
    qDebug() << QString("You turned, but there are no gumballs");
}

void SoldOutState::dispense()
{
    qDebug() << QString("No gumball dispensed");
}

void SoldOutState::refill()
{
    gumballMachine->setState(gumballMachine->getNoQuarterState());
}
