#include "SoldState.h"
#include <QDebug>

SoldState::SoldState(GumballMachine *gumballMachine)
{
    this->gumballMachine = gumballMachine;
}

void SoldState::insertQuarter()
{
    qDebug() << QString("Please wait, we're already giving you a gumball");
}

void SoldState::ejectQuarter()
{
    qDebug() << QString("Sorry, you already turned the crank");
}

void SoldState::turnCrank()
{
    qDebug() << QString("Turning twice doesn't get you another gumball!");
}

void SoldState::dispense()
{
    gumballMachine->releaseBall();
    if (gumballMachine->getCount() > 0)
    {
        gumballMachine->setState(gumballMachine->getNoQuarterState());
    }
    else
    {
        qDebug() << QString("Oops, out of gumballs!");
        gumballMachine->setState(gumballMachine->getSoldOutState());
    }
}

void SoldState::refill()
{

}
/*
QString SoldState::toString()
{
    return "dispensing a gumball";
}
*/
