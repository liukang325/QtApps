#ifndef WINNERSTATE_H
#define WINNERSTATE_H

#include "State.h"
#include "GumballMachine.h"

class WinnerState : public State
{
public:
    WinnerState(GumballMachine *gumballMachine);
    virtual void insertQuarter();
    virtual void ejectQuarter();
    virtual void turnCrank();
    virtual void dispense();
    virtual void refill();
/*
    public String toString() {
        return "despensing two gumballs for your quarter, because YOU'RE A WINNER!";
    }
*/
private:
    GumballMachine *gumballMachine;
};

#endif // WINNERSTATE_H
