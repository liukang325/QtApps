#-------------------------------------------------
#
# Project created by QtCreator 2014-06-02T22:08:08
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = QGumballMachine
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

PRECOMPILED_HEADER +=
SOURCES += main.cpp \
    HasQuarterState.cpp \
    GumballMachine.cpp \
    SoldOutState.cpp \
    NoQuarterState.cpp \
    SoldState.cpp \
    WinnerState.cpp \
    Object.cpp

HEADERS += \
    State.h \
    HasQuarterState.h \
    GumballMachine.h \
    SoldOutState.h \
    NoQuarterState.h \
    SoldState.h \
    WinnerState.h \
    Object.h
