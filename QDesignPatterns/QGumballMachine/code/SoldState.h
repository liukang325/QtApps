#ifndef SOLDSTATE_H
#define SOLDSTATE_H

#include "State.h"
#include "GumballMachine.h"

class SoldState : public State
{
public:
    SoldState(GumballMachine *gumballMachine);

    virtual void insertQuarter();
    virtual void ejectQuarter();
    virtual void turnCrank();
    virtual void dispense();
    virtual void refill();
    //QString toString();

private:
    GumballMachine *gumballMachine;
};

#endif // SOLDSTATE_H
