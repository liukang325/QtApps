#include "NoQuarterState.h"
#include <QDebug>

NoQuarterState::NoQuarterState(GumballMachine *gumballMachine)
{
    this->gumballMachine = gumballMachine;
}

void NoQuarterState::insertQuarter()
{
    qDebug() << "You inserted a quarter";
    gumballMachine->setState(gumballMachine->getHasQuarterState());
}

void NoQuarterState::ejectQuarter()
{
    qDebug() << "You haven't inserted a quarter";
}

void NoQuarterState::turnCrank()
{
    qDebug() << "You turned, but there's no quarter";
 }

void NoQuarterState::dispense()
{
    qDebug() << "You need to pay first";
}

void NoQuarterState::refill()
{

}
/*
QString toString() {
    return "waiting for quarter";
}
*/
