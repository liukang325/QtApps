#ifndef NOQUARTERSTATE_H
#define NOQUARTERSTATE_H

#include "State.h"
#include "GumballMachine.h"

class NoQuarterState : public State
{
public:
    NoQuarterState(GumballMachine *gumballMachine);
    virtual void insertQuarter();
    virtual void ejectQuarter();
    virtual void turnCrank();
    virtual void dispense();
    virtual void refill();
    //QString toString();

private:
    GumballMachine *gumballMachine;
};

#endif // NOQUARTERSTATE_H
