#include "GumballMachine.h"
#include "SoldOutState.h"
#include "NoQuarterState.h"
#include "HasQuarterState.h"
#include "SoldState.h"
#include "WinnerState.h"
#include <QDebug>

GumballMachine::GumballMachine(int numberGumballs)
{
    soldOutState = new SoldOutState(this);
    noQuarterState = new NoQuarterState(this);
    hasQuarterState = new HasQuarterState(this);
    soldState = new SoldState(this);
    winnerState = new WinnerState(this);

    state = soldOutState;
    this->count = numberGumballs;
    if (numberGumballs > 0)
    {
        state = noQuarterState;
    }
}

void GumballMachine::insertQuarter()
{
    state->insertQuarter();
}

void GumballMachine::ejectQuarter()
{
    state->ejectQuarter();
}

void GumballMachine::turnCrank()
{
    state->turnCrank();
    state->dispense();
}

void GumballMachine::setState(State *state)
{
    this->state = state;
}

void GumballMachine::releaseBall()
{
    qDebug() << QString("A gumball comes rolling out the slot...");
    if (count != 0) {
        count = count - 1;
    }
}

int GumballMachine::getCount() {
    return count;
}

void GumballMachine::refill(int count) {
    this->count += count;
    qDebug() << QString("The gumball machine was just refilled; it's new count is: " + this->count);
    state->refill();
}

State* GumballMachine::getState() {
    return state;
}

State* GumballMachine::getSoldOutState() {
    return soldOutState;
}

State* GumballMachine::getNoQuarterState() {
    return noQuarterState;
}

State* GumballMachine::getHasQuarterState() {
    return hasQuarterState;
}

State* GumballMachine::getSoldState() {
    return soldState;
}

State* GumballMachine::getWinnerState() {
    return winnerState;
}
/*
QString GumballMachine::toString() {
    StringBuffer result = new StringBuffer();
    result.append("\nMighty Gumball, Inc.");
    result.append("\nJava-enabled Standing Gumball Model #2004");
    result.append("\nInventory: " + count + " gumball");
    if (count != 1) {
        result.append("s");
    }
    result.append("\n");
    result.append("Machine is " + state + "\n");
    return result.toString();
}
*/
