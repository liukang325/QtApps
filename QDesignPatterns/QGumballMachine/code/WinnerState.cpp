#include "WinnerState.h"
#include <QDebug>

WinnerState::WinnerState(GumballMachine *gumballMachine)
{
    this->gumballMachine = gumballMachine;
}

void WinnerState::insertQuarter()
{
    qDebug() << QString("Please wait, we're already giving you a Gumball");
}

void WinnerState::ejectQuarter()
{
    qDebug() << QString("Please wait, we're already giving you a Gumball");
}

void WinnerState::turnCrank()
{
    qDebug() << QString("Turning again doesn't get you another gumball!");
}

void WinnerState::dispense()
{
    gumballMachine->releaseBall();
    if (gumballMachine->getCount() == 0)
    {
        gumballMachine->setState(gumballMachine->getSoldOutState());
    }
    else
    {
        gumballMachine->releaseBall();
        qDebug() << QString("YOU'RE A WINNER! You got two gumballs for your quarter");
        if (gumballMachine->getCount() > 0)
        {
            gumballMachine->setState(gumballMachine->getNoQuarterState());
        }
        else
        {
            qDebug() << QString("Oops, out of gumballs!");
            gumballMachine->setState(gumballMachine->getSoldOutState());
        }
    }
}

void WinnerState::refill()
{

}
