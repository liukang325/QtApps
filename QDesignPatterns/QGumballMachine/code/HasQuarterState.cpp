#include "HasQuarterState.h"
#include <QDebug>

HasQuarterState::HasQuarterState(GumballMachine *gumballMachine)
{
       this->gumballMachine = gumballMachine;
}

void HasQuarterState::insertQuarter()
{
    qDebug() << "You can't insert another quarter";
}

void HasQuarterState::ejectQuarter()
{
    qDebug() << "Quarter returned";
    gumballMachine->setState(gumballMachine->getNoQuarterState());
}

void HasQuarterState::turnCrank()
{
    qDebug() << "You turned...";
/*
    int winner = randomWinner.nextInt(10);
    if ((winner == 0) && (gumballMachine->getCount() > 1)) {
        gumballMachine->setState(gumballMachine->getWinnerState());
    } else {
        gumballMachine->setState(gumballMachine->getSoldState());
    }
    */
gumballMachine->setState(gumballMachine->getSoldState());
}

void HasQuarterState::dispense()
{
    qDebug() << "No gumball dispensed";
}

void HasQuarterState::refill()
{

}

/*
QString toString() {
    return "waiting for turn of crank";
}
*/
