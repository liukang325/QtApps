#ifndef HASQUARTERSTATE_H
#define HASQUARTERSTATE_H

#include "State.h"
#include "GumballMachine.h"

class HasQuarterState : public State
{
public:
    HasQuarterState(GumballMachine *gumballMachine);
    virtual void insertQuarter();
    virtual void ejectQuarter();
    virtual void turnCrank();
    virtual void dispense();
    virtual void refill();
    //QString toString();

private:
    GumballMachine *gumballMachine;
    //Random randomWinner = new Random(System.currentTimeMillis());
};

#endif // HASQUARTERSTATE_H
